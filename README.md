# Omall

#### 介绍

基于 Spring + SpringMVC + Mybatis 的线上购物商城

1.  前端：Html+css+JavaScript --> jsp （直接扒下的tmall前端，改写为jsp）

2.  后端：典型的CRUD维护管理模型（页面设计依旧直接找到改的）

 :collision: 成员：徐畅，顾钰，周春银，韩旭，肖千一


#### 安装教程

1.  IDEA使用maven打开
2.  增添pom.xml依赖
3.  数据库初始化
4.  配置database
5.  部署于tomcat


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


